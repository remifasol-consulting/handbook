# Workflow de développement avec Gitlab

Ce document décrit un workflow que l'on peut mettre en place pour cadrer le développement d'un projet git, en utilisant à notre avantage les outils de Gitlab. La plupart des autres providers de repo Git en ligne (Github, Bitbucket, ...) proposent des outils similaires permettant aussi la mise en place du workflow.

Je m'insipire du Gitlab Flow qui est une méthodologie de travail sur un repository de dev géré via Git. C'est une alternative à Git Flow dans laquelle on se base sur les issues, merge requests et autres concepts implémenté dans Gitlab pour organiser le travail autour du projet. J'ignore ici la partie déploiement du Gitlab flow, qui traite des production, environnement et release branches. Se réferer à la documentation Gitlab pour plus de détails:

Référence: [Introduction to GitLab Flow | GitLab](https://docs.gitlab.com/ee/topics/gitlab_flow.html) ⏳ 19m

La méthodologie simplifiée que je décrit ici peut être appliquée seul ou en équipe. Elle permet de garder un historique de repository propre et d'avoir une suite d'étapes à suivre assez claire pour l'intégration de code sur la branche principale.

L'historique de commit d'un repo constitue son histoire. N'importe quel développeur peut comprendre l'histoire d'un projet à partir de ses commits. Mais si les commits sont sales, cet historique sera très difficile à lire et à exploiter. Il est donc important d'essayer de le maintenir le plus propre possible tout au long de la vie du projet.

Bien que j'utilise les issues et merge requests de Gitlab pour le tracking, il est tout à fait possible de n'utiliser que des concepts primaire de Git comme les branches qu'on peut merger manuellement sur la branche principale.

L'avantage de Gitlab est surtout le suivi, le formalisme, et surtout la CI/CD lorsqu'elle est implémentée qui pourra s'executer automatiquement sur chaque merge request. Il est possible de faire la même chose sur Github en utilisant des pull request (même concept que les merge request de Gitlab), et des Github Action ou n'importe quel autre système compatible pour la CI/CD.

## Création du repository

On créé le repository localement dans un dossier et un commit vide afin d'avoir une racine "propre" pour toute l'arborescence de commit:

```bash
git init
git commit --allow-empty -m "Initial empty commit"
```

On créé un repository vide sur Gitlab ou Github et on récupère l'url SSH du repo, de la forme `git@gitlab.com:GROUP/REPO.git`. On ajoute cette addresse en remote de notre repo local puis on push la branche principale `main`:

```bash
git remote add origin git@gitlab.com:GROUP/REPO.git
git push -u origin main
```

Pour rappel, l'option `-u` permet d'indiquer que notre branche `main` locale doit "suivre" la branche `main` du remote `origin` (dont le nom est `origin/main` dans notre repo local). Après avoir fait une première fois le push avec l'option `-u` on pourra se contenter de faire `git push` et `git pull` car git aura configuré son tracking pour associer `main` à `origin/main` pour tous les pull et les push.

Par la suite, hors cas exceptionel, on ne travaillera jamais sur la branche `main`. L'objectif sera de maintenir son historique propre en travaillant sur des branches de feature. Elles pourront rester "sale" pendant tout le développement mais seront réécrites et nettoyés avant merge sur `main`.

## Boucle de développement

La boucle de développement est constituée des étapes suivantes:

- Création d'une issue pour décrire ce que l'on souhaite faire (ajouter une feature, régler un bug, améliorer l'architecture du code, experimenter, etc.)
- Création d'une merge request à partir de l'issue
- Checkout en local de la branche associée à la merge request
- Développement le long de la branche en tout liberté (commits sales, experimentations, plusieurs sous branches, etc)
- Finalisation du développement: rebase sur main et réécriture de l'historique de la branche à l'aide de git pour obtenir une suite propre et cohérente de commits
  - Si CI/CD implémentée: on s'assure que tous les tests passent sur la merge request
  - Si travail en équipe: on demande aux collègue une review du code
  - Si besoin de changement: on fait les changement et on réécrit la branche si nécessaire
- Fin du dev: on passe la MR en "Ready" et on la merge sur Gitlab

Ce processus peut être rendu plus ou moins rigide en fonction des conditions du projets (projet solo vs projet en équipe, petite équipe vs open source, projet destiné à être maintenu longtemps vs projet jetable, etc)

### Création d'une issue

L'issue permet de donner un nom au développement à effectuer et de donner une description. Cela peut nous servir d'outil de prise de note, ou de communication avec les autres développeurs et utilisateurs.

Sur des projets en équipe il est fréquent que la personne qui créé une issue ne soit pas celle qui prendre en charge le développement. Un cas fréquent est l'issue de bug: un utilisateur rencontre un comportement innatendu, il va poster une issue pour décrire le problème et donner des informations, une discussion se met en place, puis un développeur finira peut être par se charger de l'issue. Sur les gros projets open source certaines issues restent ouvertes pendant des mois voir des années car il n'y a pas forcément la force de travail nécessaire pour addresser tous les problèmes / améliorations à faire.

Sur un projet d'entreprise, le développement est souvent organisé sur un modèle agile, via la méthode Scrum ou la méthode Kanban. Dans ces cas les issues sont souvent catégorisés et priorisé par l'équipe de développement en partenariat avec le product owner (la personne en charge du produit, représentant les utilisateurs). On fonctionne souvent par sprint (2 à 3 semaine de développement), et au début du sprint on décide des issues qui doivent être addressé pendant le sprint. Chaque jour chaque développeur avance sur son et ses issues, rapporte aux autres membre de l'équipe ses points de blocage et son avancement, et à la fin du sprint on fait le bilan.

Dans le cadre de ce document, l'issue sert surtout de support technique et informatif sur la nature des taches à effectuer. Gitlab nous facilite fortement la mise en place du workflow de développement en partant des issues.

### Création d'une merge request à partir de l'issue

A partir de la page de l'issue on peut directement cliquer sur "Create merge request" sur Gitlab (par contre je ne crois pas que Github le permette donc il faut aller créer la pull request et référencer l'issue dans sa description).

### Checkout en local de la branche associée à la merge request

En meme temps que la merge request, Gitlab nous créé une branche et nous propose un bouton pour récupérer les commandes git à utiliser localement pour récupérer la branche et se placer dessus. C'est de la forme suivante:

```bash
git fetch origin
git checkout -b NUMERO-NOM-DE-LA-MERGE-REQUEST origin/NUMERO-NOM-DE-LA-MERGE-REQUEST
```

On utilise donc ces commandes dans notre repo local et on est prêt à travailler. On code.

> A noter qu'on passe par une merge request pour créer la branche, mais on peut également choisir de se passer de la merge request (et de l'issue !) si on veut simplifier le process. Dans ce cas on se contentera de changer de créer une nouvelle branche depuis la branche `main`, de travailler dessus, puis de la merger manuellement plus tard. Voir dernière section en annexe pour les commandes.

### Développement le long de la branche en tout liberté (commits sales, experimentations, plusieurs sous branches, etc)

ON CODE ! On ne s'occupe pas de maintenir les commits propres à ce niveau (sauf si ça nous arrange évidement). On peut tout se permettre, on peut tout casser, on peut revenir en arrière (`git reset`), on peut meme créer de nouvelles branches depuis notre branche (`git checkout -b`) pour tester des trucs.

### Finalisation du développement

On arrive au stade ou on juge que le développement est terminé.

La première chose à vérifier est que la pipeline de CI/CD passe, si elle est implémentée pour le projet (fortement conseillé).

Si c'est le cas il est temps de nettoyer la branche. Si c'est vraiment le bordel on peut choisir de procéder en plusieurs étapes. On peut également créer une branche temporaire au cas ou on se perde et qu'on veuille rapidement revenir à l'état de départ (`git branch _tmp`, et on oubliera pas de supprimer plus tard cette branche avec `git branch -f -d _tmp`).

Les outils que l'on peut utiliser pour nettoyer une branche sont multiples et assez technique. Il est donc conseiller de se forcer à les utiliser régulierement pour être à l'aise, et de faire des sessions git d'entrainement qui ne servent à rien a part apprendre. On utilisera principalement `git checkout` pour se déplacer de commit en commit, `git reset` pour déplacer la branche, et le très puissant `git rebase -i` permettant de rejouer les commits en les éditant. La plupart du temps on utilise `git rebase -i`, mais parfois les deux autres permettent de se simplifier la vie dans des cas assez simple. En réalité une fois qu'on a bien compris comment git fonctionne et représente le repository, on peut se balader dans les commits très facilement.

Voici un tutorial interactif qui semble très bien fait pour apprendre et comprendre git: https://learngitbranching.js.org/

Voici plusieurs tutoriels pour le rebase interactif:

- [Git rebase&nbsp;: qu'est-ce que c'est&nbsp;? Comment s'en servir&nbsp;? – Miximum](https://www.miximum.fr/blog/git-rebase/) ⏳ 11m
- [A Guide to Git Interactive Rebase, with Practical Examples - SitePoint](https://www.sitepoint.com/git-interactive-rebase-guide/) ⏳ 10m
- [Git rebase | Atlassian Git Tutorial](https://www.atlassian.com/fr/git/tutorials/rewriting-history/git-rebase) ⏳ 9m
- [Git Tutorial =&gt; Interactive Rebase](https://riptutorial.com/git/example/3282/interactive-rebase) ⏳ 3m
- [How to survive as a programmer?: Advanced GIT tutorial - Interactive rebase](https://howtosurviveasaprogrammer.blogspot.com/2020/03/advanced-git-tutorial-interactive-rebase.html) ⏳ 4m

Il ne faut surtout pas négliger l'apprentissage de cette fonctionnalité. C'est probablement l'une des plus puissantes de git.

On peut effectuer le rebase interactif sur la branche de travail, sur un commit de la branche de travail (si certains commits sont déjà "propre" il est tout a fait possible de les laisser et de rebase le reste par dessus), ou sur `main`. La dernière option est a privilégier car elle permettra d'avoir moins de merges par la suite. Mais il est parfois difficile de combiner nettoyage de ses commits avec rebase sur `main`, car si d'autres développeurs ont eux même mergé sur `main` depuis le début de notre développement alors il faudra probablement résoudre des conflits. On peut donc s'y prendre en deux fois: d'abord le nettoyage en rebasant interactivement (option `-i`) sur notre propre branche de travail, puis résolution des conflits en rebasant normalement (sans l'option `-i`) sur `main`. Evidemment si on travail seul il y a peu de chance que `main` ait avancé, sauf si on travaille sur plusieurs merge requests en même temps et qu'on en a mergé d'autres depuis.

Arrivé a ce stade on doit avoir une branche propre, qui est enracinée sur `main`, et donc prette à être mergée. Si on travaille à plusieurs on demande alors à un ou plusieurs autres membres de l'équipe de reviewer notre code. Leur role est de détecter des erreurs potentielles qu'on aurait loupé, questionner notre façon de faire les choses, lancer des discussion sur le code, et s'informer des modifications apportées au code. Si nécessaire on fait des modifications, on re-nettoie la branche, et on repete jusqu'a ce que la review passe.

### Fin du dev: on passe la MR en "Ready" et on la merge sur Gitlab

Si la review est approuvée, on clique sur le bouton "Merge" (la MR doit être passée en "Ready" si ce n'est pas fait) et on contemple la beauté de la chose.

Localement on peut cleaner nos branches:

```bash
git fetch -p
git checkout main
git pull
git branch -d NUMERO-NOM-DE-LA-MERGE-REQUEST
```

et on est pret a débuter un nouveau développement: on repete le process.

## Annexes

### Fonctionner sans issue / sans merge request

L'utilisation des issues et merge request est surtout là pour cadrer le process, automatiser certaines actions via les bouton de Gitlab, et communiquer aux autres. Si on a déjà bien en main la méthodologie et qu'on travaille sur un petit projet perso, on peut se permettre de travailler uniquement avec des branches et des merges manuels. Voici les commandes:

```bash
git checkout main
git pull
git checkout -b ma-super-fonctionnalite
git push -u origin ma-super-fonctionnalite
```

A partir de la on travaille et on push notre branche, salement. A la fin du développement on la nettoie, on la rebase sur `main` si nécessaire, puis on peut la merger manuellement:

```bash
git checkout main
git merge ma-super-fonctionnalite
git push
git branch -d ma-super-fonctionnalite
git push origin --delete ma-super-fonctionnalite
```

Cette approche est plus légère et moins outillée. Elle a l'avantage d'être agnostique de la plateforme git utilisée (Gitlab, Github, Bitbucket, ...). Par contre on ne profitera pas du suivi des execution de pipeline CI/CD des merge request, ou des outils de review.

Il est néanmmoins possible d'avoir la merge request et de se passer de l'issue. Soit on la créé sur Gitlab on l'associant à notre branche. Soit la première fois qu'on push la branche git nous donnera un lien auquel on peut accéder pour créer la merge request.

### Ajouter le TDD dans la boucle

Dans une approche TDD (test driven development) on s'assure d'implémenter des tests qui échoue avant notre fonctionnalité ou bugfix, puis on implémente le code nécessaire au passage des tests, puis on refactor pour rendre le code maintenable et toujours facilement testable.

Ces 3 étapes peuvent être intégrées au workflow de développement. On pourra s'arranger pour réécrire notre branche en ayant des suites de commits de la forme suivante par exemple:

- Tests for XXX
- Resolve tests for XXX
- Refactor code for XXX

(ou XXX est a remplacer par une indication de ce qu'on veut faire).

Ce n'est bien sur pas une obligation et on peut choisir de squasher tout ça par la suite en un seul commit "Implement XXX". Chacun ses préférences, tant que l'historique est propre !
